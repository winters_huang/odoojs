### context

1. dataset.call_kw 接口 有个重要参数 centext
2. 携带一些额外信息到服务器
3. 登录后, session_info 携带 user_context

#### 修改 odoorpc/web.js 文件 Session 部分, 补充 context 的处理

```
class Session extends JsonRequest {
  static get session_info() {
    return this._session_info
  }

  static get context() {
    return this.user_context
  }

  static get user_context() {
    const session_info = this.session_info
    const context = session_info.user_context || {}
    const allowed_company_ids = this.allowed_company_ids
    return { ...context, allowed_company_ids }
  }

  static get current_company_id() {
    const { user_companies = {} } = this.session_info || {}
    const { current_company } = user_companies
    return current_company
  }

  static get allowed_company_ids() {
    return [this.current_company_id]
  }
}

```

#### 修改测试文件 odoorpc_test/testcase/dataset.js 文件

```
import BaseTestCase from './base'
import rpc from '@/odoorpc'

export default class DatasetTestCase extends BaseTestCase {
  async call_kw_with_context() {
    // ...
    const context = rpc.web.session.context
    // ...
  }
}

```
