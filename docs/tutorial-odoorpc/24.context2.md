### context

#### 使用指南

1. 用户登陆后, 获取 rpc.env.context
2. 调用 Model 方法时, 默认取 rpc.env.context
3. 可以在 调用 Model 方法, 设置特定的 context

#### 修改 odoorpc/env.js 文件

```
export class Environment {
  constructor(payload = {}) {
    const { context } = payload

    if (context) this._context = context
    else {
      const ctx = web.session.user_context
      this._context = ctx
    }
  }

  get context() {
    return this._context
  }

  with_context(kwargs = {}, context) {
    const context2 = context ? context : this.context
    const context3 = { ...context2, ...kwargs }
    const env = new this.constructor({ context: context3 })
    return env
  }

  copy(context) {
    const env = new this.constructor({ context })
    return env
  }

  // ...

}
```

#### 修改 odoorpc/models.js 文件

```
export class MetaModel {
  static with_context(kwargs = {}, context) {
    const context2 = context ? context : this.env.context
    const context3 = { ...context2, ...kwargs }
    return this.with_env(this.env.copy(context3))
  }

  static with_env(env) {
    const OldModel = this
    class NewModel extends OldModel {
      constructor(...args) {
        super(...args)
      }
    }

    NewModel._env = env
    return NewModel
  }
}

```

#### 新增测试文件 odoorpc/test/addons/context.js 文件

```
import BaseTestCase from './base'
import rpc from '@/odoorpc'

export default class ModelTestCase extends BaseTestCase {
  async test() {
    await this.with_context()
    await this.with_env()
    await this.env_copy()
  }

  async with_context() {
    await this.login()
    const model = 'ir.module.module'
    const Model2 = rpc.env.model(model)
    const res2 = await Model2.name_search({ limit: 8 })
    console.log('cn module', res2)

    const lang = 'en_US'
    //   const lang = 'zh_CN'
    const ctx = { lang }
    const Model = Model2.with_context(ctx)
    const res = await Model.name_search({ limit: 8 })
    console.log(res)
  }

  async with_env() {
    await this.login()
    const model = 'ir.module.module'
    const Model2 = rpc.env.model(model)
    const res2 = await Model2.name_search({ limit: 8 })
    console.log('cn', res2)

    const lang = 'en_US'
    //   const lang = 'zh_CN'
    const ctx = { lang }
    const env = rpc.env.with_context(ctx)
    const Model = Model2.with_env(env)
    const res = await Model.name_search({ limit: 8 })
    console.log(res)
  }

  async env_copy() {
    await this.login()
    const model = 'ir.module.module'
    const Model2 = rpc.env.model(model)
    const res2 = await Model2.name_search({ limit: 8 })
    console.log('cn', res2)

    const context2 = rpc.env.context
    const lang = 'en_US'
    //   const lang = 'zh_CN'
    const context = { ...context2, lang }
    const env = rpc.env.copy(context)
    const Model = env.model(model)
    const res = await Model.name_search({ limit: 8 })
    console.log(res)
  }
}


```
