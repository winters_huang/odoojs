### odooapi 介绍

1. 依赖 odoorpc
2. 集成 odoorpc 的所有功能
3. 扩展 Action, Views, Node

### Action

1. Action.load 方法
2. Action.load_views
3. load 和 load_views 共同的返回结果 info = {action, context, views}
4. 该三个参数 为后续接口函数的基本参数

### Views

1. Views.search.default_value 同步函数, 获取默认搜索条件
2. Views.list.web_search_read 异步函数, 获取 list view 的数据
3. Views.form.read 异步函数, 获取 form view 的数据
