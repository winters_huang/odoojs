### RPC

#### 目标

1. 标准化 odoorpc 的对外接口规范
2. 便于扩展, 增加新的接口.

#### odoorpc 对外接口规范

1. 导入方法: import api from '@/odoorpc'
2. 在本教程中导入方法为: import api from '@/api_demo/odoorpc_t3'
3. 调用接口方法: api.web.webclient.version_info
4. 该接口对应请求: /web/webclient/version_info

#### 步骤

1. 复制文件夹 api_demo/odoorpc_t2, 命名为 api_demo/odoorpc_t3
2. 文件 api_demo/odoorpc_t3/request.js 已经同步复制
3. 创建 文件 api_demo/odoorpc_t3/index.js
4. api_demo/odoorpc_t3/index.js 中, 实现上述 odoorpc 对外接口规范
5. 创建测试页面 components/Test3Rpc.vue, 可由 components/Test2Request.vue 复制
6. 修改 app.vue, 引入 components/Test3Rpc.vue
7. 修改 Test3Rpc.vue 引入 odoorpc
8. 修改 components/Test3Rpc.vue 的 按钮点击事件函数, 以标准规范调用接口

#### api_demo/odoorpc_t3/index.js 的内容

1. 标准化 odoorpc 的对外接口规范
2. 规范代码, 便于扩展新的接口

```
import { JsonRequest } from './request'

class Webclient extends JsonRequest {
  constructor(payload) {
    super(payload)
  }

  static async version_info() {
    const url = '/web/webclient/version_info'
    return this.json_call(url, {})
  }
}

class Session extends JsonRequest {
  constructor(payload) {
    super(payload)
  }

  static async authenticate({ db, login, password }) {
    const url = '/web/session/authenticate'
    const payload = { db, login, password }
    const session_info = await this.json_call(url, payload)
    return session_info
  }
}

class Home extends JsonRequest {
  constructor(payload) {
    super(payload)
  }

  static async tests_mobile() {
    const url = '/web/tests/mobile'
    return await this.http_call(url, {})
  }

  static async benchmarks() {
    const url = '/web/benchmarks'
    return await this.http_call(url, {})
  }
}

const web = Home
web.webclient = Webclient
web.session = Session

export class RPC {
  constructor() {}

  static init({ baseURL, timeout }) {
    JsonRequest.baseURL = baseURL
    JsonRequest.timeout = timeout
  }
}

const rpc = RPC
rpc.web = web

export default rpc

```

#### components/Test3Rpc.vue 的内容 如下:

```
<script>
// import api from '@/odoorpc'
import api from '@/api_demo/odoorpc_t3'

const baseURL = process.env.VUE_APP_BASE_API
const timeout = 50000
api.init({ baseURL, timeout })

export default {
  // ...

  methods: {
    async onclick() {
      console.log('click btn')
      const result = await api.web.webclient.version_info()
      console.log('result:', result)
      this.result = result
      this.error = { data: {} }
    }
  }
}
```
