### request

#### 目标

1. 减少重复代码. axios 和 JsonRPC 相关的基础处理, 统一处理
2. 便于扩展, 如文件上传下载的接口需要特别的 axios 配置.

#### 步骤

1. 创建文件夹 api_demo
2. 创建文件夹 api_demo/odoorpc_t2
3. 创建文件 api_demo/odoorpc_t2/request.js
4. 编辑 api_demo/odoorpc_t2/request.js 的内容
5. 调用 request 的方法: import { JsonRequest } from '@/api_demo/odoorpc_t2/request'
6. 创建测试页面 components/Test2Request.vue
7. 修改 app.vue, 引入 components/Test2Request.vue
8. 修改 components/Test2Request.vue 的 按钮点击事件函数
9. 通过 JsonRequest 发送请求

#### 创建文件 api_demo/odoorpc_t2/request.js

1. 创建文件夹 api_demo
2. 创建文件夹 api_demo/odoorpc_t2
3. 创建文件 api_demo/odoorpc_t2/request.js

```
cd src
mkdir api_demo
cd api_demo
mkdir odoorpc_t2
cd odoorpc_t2
touch request.js
```

#### api_demo/odoorpc_t2/request.js 的内容

1. 存储 axios 的基础配置参数 baseURL / timeout
2. JsonRpc 请求格式的处理
3. 其他特定的配置, 如图片上传下载接口的特殊配置
4. request.js 文件可查看教程项目. 下面是该文件的关键结构

```
export class JsonRequest {
  static init({ baseURL, timeout }) {}
  static async json_call(url, payload = {}) {}
}

```

#### Test1Connect.vue 的内容 如下:

```
<script>
import { JsonRequest } from '@/api_demo/odoorpc_t2/request'

// const baseURL = 'http://192.168.56.108:8069'
const baseURL = process.env.VUE_APP_BASE_API
const timeout = 50000
JsonRequest.init({ baseURL, timeout })

export default {
  // ...

  methods: {
    async onclick() {
      console.log('click btn')
      const url = '/web/webclient/version_info'
      const result = JsonRequest.json_call(url, {})
      console.log('result:', result)
      this.result = result
      this.error = { data: {} }
    }
  }
}
```
